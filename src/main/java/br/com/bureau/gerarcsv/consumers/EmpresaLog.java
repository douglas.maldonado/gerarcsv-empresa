package br.com.bureau.gerarcsv.consumers;

import br.com.bureau.cadastro.models.Empresa;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

@Component
public class EmpresaLog {

    @KafkaListener(topics = "spec-douglas-maldonado-3", groupId = "douglas-1")
    public void receber(@Payload Empresa empresa)
            throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        montaArquivoCsv(empresa);

    }

    private void montaArquivoCsv(Empresa empresa)
            throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

        Writer writer = new FileWriter("/home/a2/aula-spring/api-acesso/LogAcesso.csv", true );
        StatefulBeanToCsv<Empresa> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(empresa);

        writer.flush();
        writer.close();
    }


}

